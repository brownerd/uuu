+++
title = "About"
description = "About UUU"
+++

## Goals

UUU evolved as an effort to make the process of hacking a theme easier. I think this makes that possible. I hope this opens insights into making theme creation, editing and sharing easier for everyone.


## History
This project started out as an effort to make a theme that was more based on Typographical styling than lots of color coordination. Along the way I figured out that I could hack a lot of other parts of Atom too. So, I made the PacMan cursor and then I figured out how to use the settings panel to edit a theme. There was a megaton of CSS to write, but it was fun to take a deep dive into what LESS can do.

## UUU
I don't know what UUU want out of an editor, but UUU should be able to easily crank out or alter a theme how ever UUU want. Personally, I just want to work from an editor that is fun to work in and easy to customize. Even though I use VSCode and Sublime still, my favorite editor to work from is Atom. That's is. Stay awesome!
