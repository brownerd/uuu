+++
title = "About"
description = "About UUU"
+++

## Issues

If you run into any issues post them [here](https://gitlab.com/brownerd/uuu/issues)

## Crash bug

There is one setup that Atom doesn't like. Just watch this video to avoinf this edge case.

<div class="youtube resize">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/PlE-_UINK9Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
