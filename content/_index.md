+++
title = "Home"
description = "Sara Soueidan — Front-end web developer, author and speaker"
+++

<h2 class="theme__heading">Get started</h2>

Open your terminal and run the line below

`apm install uuu-ui uuu-syntax uuu-kit`

- In Atom, press <kbd>cmd</kbd> <kbd>,</kbd> to open settings
- Change themes to UUU-ui and UUU-syntax
- Click on the Settings icon (gear) get UUUr style on!

<h2 class="theme__heading">Features</h2>



- Pac-Man Cursor <div class="pacman"></div> &#183; &#183; &#183;
- Punk Gutter
- Full Theme control from settings
- Import/Export themes
- Save your own themes
- Quickly switch between themes
- Sync Syntax and UI font sizes
- Hack any UUU theme UUU create or install
