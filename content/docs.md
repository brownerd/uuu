+++
title = "Docs"
description = "UUU theme documentation"
+++

## Installation

Open your terminal and run the line below

`apm install uuu-ui uuu-syntax uuu-kit`


<div class="youtube resize">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/TNuIubWXVjc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>


## Overview

<div class="youtube resize">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/C2N2O3WR-3U" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>


## Usage

This package does several things:

1. Make Syntax and UI themes for Atom
1. Imports and Exports UI and SYNTAX made with UUU
1. Syncs font-sizes between UI and SYNTAX
1. Enables you to switch between preset UUU themes
1. Enables you to save and switch between your own themes
1. Makes it easy to Reload the window


## Switching between preset themes

<div class="youtube resize">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/B-FgipJ2n_M" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

There are 10 preset themes. Here is how you access them:

1. **Theme 1 - Melowdrama**     Press <kbd>ctrl</kbd> <kbd>opt</kbd> <kbd>cmd</kbd> <kbd>1</kbd>
1. **Theme 2 - Amir**           Press <kbd>ctrl</kbd> <kbd>opt</kbd> <kbd>cmd</kbd> <kbd>2</kbd>
1. **Theme 3 - Schmaterial**    Press <kbd>ctrl</kbd> <kbd>opt</kbd> <kbd>cmd</kbd> <kbd>3</kbd>
1. **Theme 4 - DarkEarth**      Press <kbd>ctrl</kbd> <kbd>opt</kbd> <kbd>cmd</kbd> <kbd>4</kbd>
1. **Theme 5 - Brownerd**       Press <kbd>ctrl</kbd> <kbd>opt</kbd> <kbd>cmd</kbd> <kbd>5</kbd>
1. **Theme 6 - SuperNES**       Press <kbd>ctrl</kbd> <kbd>opt</kbd> <kbd>cmd</kbd> <kbd>6</kbd>
1. **Theme 7 - TMNT-Donetello** Press <kbd>ctrl</kbd> <kbd>opt</kbd> <kbd>cmd</kbd> <kbd>7</kbd>
1. **Theme 8 - MiamiVice**      Press <kbd>ctrl</kbd> <kbd>opt</kbd> <kbd>cmd</kbd> <kbd>8</kbd>
1. **Theme 9 - Typographical**  Press <kbd>ctrl</kbd> <kbd>opt</kbd> <kbd>cmd</kbd> <kbd>9</kbd>
1. **Theme 10 - BuckNaked**     Press <kbd>ctrl</kbd> <kbd>opt</kbd> <kbd>cmd</kbd> <kbd>.</kbd>

You can also use the Menu. Goto Packages > uuu. Then select the theme you want.

All of these themes are easily hackable using UUU. Use any of these as a starting point. You do not need to use these. But you will need to make your own or used one from someone else.


## Theming Setup

<div class="youtube resize">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/Q6B28iQ8iD0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

## Sample languages
Download [uuu-languages](https://github.com/brownerd/uuu-languages/archive/master.zip) so you can easily switch between multiple languages while you're theming.



## Saving your themes

<div class="youtube resize">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/B-FgipJ2n_M?start=472" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

As you make changes to ANY theme, it's state will be saved behind the scenes, but once you switch away to another theme, the previous changes will be lost. So here is how you can save your own themes so they wont get lost when you switch to another theme.

1. Work on your UI and SYNTAX theme for as long as you want
1. Press <kbd>ctrl</kbd> <kbd>opt</kbd> <kbd>1</kbd> to save your custom theme.

Notice that you can also use the Menu as well Packages > uuu > Save Theme.

You have 5 slots to save to.


## Using your saved themes

Once you have a custom theme saved to one of the available slots, you can then switch to another theme without losing your work. When you are ready to switch back to your custom theme you can use a hotkey or use the Menu above Packages > uuu > Use Theme.

Example: If you save your custom theme to Slot 1, then you can access by hotkey by pressing <kbd>ctrl</kbd> <kbd>1</kbd>

*If you switch to an empty slot, you will get an alert inside the Atom window. Telling you to save a theme to the slot first.*


## Exporting / Importing

<div class="youtube resize">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/ah1W2W5ea_k" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

### Export both Syntax and UI
Press <kbd>ctrl</kbd> <kbd>opt</kbd> <kbd>cmd</kbd> <kbd>o</kbd> - This will reveal a "syntax+ui theme object"
*You can also goto Packages > uuu > Export theme*


### Import Syntax and UI
1. Paste a "syntax+ui theme object" into any open window in atom
1. Then select the click and drag to highlight the text you just pasted in.
1. Press <kbd>ctrl</kbd> <kbd>opt</kbd> <kbd>o</kbd> - This will import the "syntax+ui theme object" and reset to the new theme.
*You can also goto Packages > uuu > Import theme*



## Extra features

### Font Check

It is common for the font-size of the UI to be different from the font-size of the SYNTAX.

Press <kbd>ctrl</kbd> <kbd>opt</kbd> <kbd>cmd</kbd> <kbd>c</kbd> to see what they are. I prefer to have them in sync, but other's might want. If you want to sync the UI and SYNTAX, then follow the steps below.

### Font Sync

<div class="youtube resize">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/_HmFAEO8swM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

To sync the UI and SYNTAX font-sizes to be the same:

Press <kbd>ctrl</kbd> <kbd>opt</kbd> <kbd>cmd</kbd> <kbd>f</kbd>

### Reload

Reloading the window can be a bit of a pain. Here is a hotkey to make it easy:

Press <kbd>ctrl</kbd> <kbd>opt</kbd> <kbd>cmd</kbd> <kbd>r</kbd>


## Issues
If your style doesn't show, or it if looks like all the styles reverted to the default, then just "reload" the window. <kbd>ctrl</kbd> <kbd>opt</kbd> <kbd>cmd</kbd> <kbd>r</kbd>


## Extras
Here a few packages and a font that I like to use.

-  [File Icons](https://atom.io/packages/file-icons).
-  Terminal for Atom [Platformio IDE Terminal](https://atom.io/packages/platformio-ide-terminal).
-  [Monoid](https://larsenwork.com/monoid/).


## Goals

This evolved as an effort to make the process of hacking a theme easier. I think this makes that possible. I hope this opens insights into making theme creation, editing and sharing easier for everyone.


## Bugs
If there is a special case that you need, then let me know. I can't promise that I can make an update, but I will look at the ticket and respond. Post bugs, issue and requests [here](https://gitlab.com/brownerd/uuu/issues)
