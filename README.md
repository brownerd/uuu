# UUU
Just some notes for later use

ci

## hugo cli
run rev server
`hugo server -D --minify`

`hugo server --disableFastRender`

`hugo server -D --minify --disableFastRender`


`hugo new themes/darkness`

## atom cli
`apm publish patch`

## Scratch pad

```html
{{ range where .Data.Pages "Title" "Home"}}

{{ .Content }}
<h4>no blah</h4>

{{end}}

```
```html

{{ $indexjs := resources.Get "js/index.mjs" }}
{{ $testjs := resources.Get "js/test.js" }}
{{ $js := slice $indexjs | resources.Concat "js/bundle.js" }}

<script type="module" src="{{ $js.Permalink }}" async></script>
<!-- <script nomodule src="fallback.js"></script> -->
```


```toml

[mediaTypes]
  [mediaTypes."text/javascript"]
    suffixes = ["mjs", "js"]

```


### resources
- [hugo pipes and asset processing](https://regisphilibert.com/blog/2018/07/hugo-pipes-and-asset-processing-pipeline/)
- [Have Different Portions Of Code For Production And Development](https://simpleit.rocks/golang/hugo/avoid-loading-portions-of-code-locally-in-hugo/)
- [pika/web](https://github.com/pikapkg/web)
